import Head from 'next/head'

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <Head>
        <title>Salonflix - Salao 1</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        
        <div className="flex">
          <img src="/capa.png" alt="" />

        </div>

        <h2 className="text-4xl font-bold">SERVIÇOS</h2>

        
        <div class="grid gap-4 border p-12 rounded-lg">
          <div class="flex gap-8">
            <div>
              <img src="/barba.png" alt="" />
            </div>
            <div class="grid gap-4">
              <div className="text-left">
                <h3 className="font-bold text-left">Barba + Cabelo</h3>
                <p className="">Promoção Corte Combo</p>
                <p className="">Exclusivamente para horários</p>
              </div>
              <div class="border-b border-black"></div>
              <div class="text-left">
                <p>Segunda a Quarta-feira das 10h às 16hs</p>
                <p>Salão do Milhão - Rua das Flores, 22 - (12) 3456-7890</p>
              </div>
            </div>
          </div>

          <div class="flex">
            <div class="">
              <label class=" grid grid-cols-3 gap-4 p-2 border rounded-md" htmlFor="10">
                <p>1 x ao mês - R$ 85,50 10% off</p>
                <input type="radio" id="10" name="10" value="10" />
              </label>
              <div className="">
              </div>
            </div>
          </div>
          <div class="flex">
            <div class="">
              <label class=" grid grid-cols-3 gap-4 p-2 border rounded-md" htmlFor="10">
                <p>1 x ao mês - R$ 85,50 10% off</p>
                <input type="radio" id="10" name="10" value="10" />
              </label>
              <div className="">
              </div>
            </div>
          </div>
          <div class="flex">
            <div class="">
              <label class=" grid grid-cols-3 gap-4 p-2 border rounded-md" htmlFor="10">
                <p>1 x ao mês - R$ 85,50 10% off</p>
                <input type="radio" id="10" name="10" value="10" />
              </label>
              <div className="">
              </div>
            </div>
          </div>
          <div class="flex">
            <div class="">
              <label class=" grid grid-cols-3 gap-4 p-2 border rounded-md" htmlFor="10">
                <p>1 x ao mês - R$ 85,50 10% off</p>
                <input type="radio" id="10" name="10" value="10" />
              </label>
              <div className="">
              </div>
            </div>
          </div>

        </div>

      </main>

      <footer className="flex items-center justify-center w-full h-24 border-t">
        <a
          className="flex items-center justify-center"
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className="h-4 ml-2" />
        </a>
      </footer>
    </div>
  )
}
